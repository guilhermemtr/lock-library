#!/bin/bash
CC=g++
RM=rm -f

SRC_PATH=src
INC_PATH=include

CFLAGS=-Wall $(MODE) -I$(INC_PATH)
LDFLAGS=-lpthread -lm

SOURCES=$(SRC_PATH)/scoped_lock.cc $(SRC_PATH)/scoped_rwlock.cc main.cc
OBJECTS=$(SOURCES:.cc=.o)

EXECUTABLE=test

MODE=$(RELEASE)

RELEASE=-O2
DEBUG=-g 

all: $(SOURCES) $(EXECUTABLE)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)

$(EXECUTABLE): $(OBJECTS)

%.o: %.cc
	$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	$(RM) $(OBJECTS) $(EXECUTABLE)
